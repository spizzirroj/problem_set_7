
R version 3.3.2 (2016-10-31) -- "Sincere Pumpkin Patch"
Copyright (C) 2016 The R Foundation for Statistical Computing
Platform: x86_64-apple-darwin13.4.0 (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

[R.app GUI 1.68 (7288) x86_64-apple-darwin13.4.0]

[History restored from /Users/James/.Rapp.history]

> #experiment 1
> subjects<-24
> trials<-6
> mu1<-27.2/36 #mean accuracy for nonword condition
> mu2<-22.3/36 #mean accuracy for part-word condition
> highlowdiff<-(.79-.72) #difference in accuracy between high transition-probability words and low transition-probability words
> 
> #function for creating data for a subject
> gensub<-function(subject,trials,condition,mu.high,mu.low){
+ 	#subject = subject id
+ 	#trials = how many trials per subject?
+ 	#condition = what condition was this subject run in?
+ 	#mu.high, mu.low = average accuracy for high transition-probability words and low transition-probability words
+ 	subdat<-data.frame(subject=rep(subject,trials),
+ 		trial=1:6, 
+ 		condition=rep(condition,trials),
+ 		transition=c(1.0,.75,.75,.5,.41,.37),
+ 		correct=c(replicate(n=trials/2, rbinom(n=1,size=1,prob=mu.high)),replicate(n=trials/2, rbinom(n=1,size=1,prob=mu.low))))
+ 	return(subdat)
+ }
> 
> #ok, let's draw some simulated data
> exp1<-gensub(1,trials,"nonword",mu1+highlowdiff/2,mu1-highlowdiff/2)
> for (i in 2:(subjects/2)){
+ 	exp1<-rbind(exp1,gensub(i,trials,"nonword",mu1+highlowdiff/2,mu1-highlowdiff/2))
+ }
> for (i in (subjects/2+1):subjects){
+ 	exp1<-rbind(exp1,gensub(i,trials,"partword",mu2+highlowdiff/2,mu2-highlowdiff/2))
+ }
> 
> exp1
    subject trial condition transition correct
1         1     1   nonword       1.00       1
2         1     2   nonword       0.75       1
3         1     3   nonword       0.75       0
4         1     4   nonword       0.50       1
5         1     5   nonword       0.41       1
6         1     6   nonword       0.37       1
7         2     1   nonword       1.00       1
8         2     2   nonword       0.75       1
9         2     3   nonword       0.75       0
10        2     4   nonword       0.50       1
11        2     5   nonword       0.41       1
12        2     6   nonword       0.37       0
13        3     1   nonword       1.00       0
14        3     2   nonword       0.75       1
15        3     3   nonword       0.75       1
16        3     4   nonword       0.50       0
17        3     5   nonword       0.41       1
18        3     6   nonword       0.37       1
19        4     1   nonword       1.00       0
20        4     2   nonword       0.75       1
21        4     3   nonword       0.75       1
22        4     4   nonword       0.50       1
23        4     5   nonword       0.41       1
24        4     6   nonword       0.37       1
25        5     1   nonword       1.00       0
26        5     2   nonword       0.75       1
27        5     3   nonword       0.75       1
28        5     4   nonword       0.50       0
29        5     5   nonword       0.41       1
30        5     6   nonword       0.37       1
31        6     1   nonword       1.00       1
32        6     2   nonword       0.75       0
33        6     3   nonword       0.75       1
34        6     4   nonword       0.50       0
35        6     5   nonword       0.41       1
36        6     6   nonword       0.37       1
37        7     1   nonword       1.00       1
38        7     2   nonword       0.75       0
39        7     3   nonword       0.75       1
40        7     4   nonword       0.50       1
41        7     5   nonword       0.41       1
42        7     6   nonword       0.37       1
43        8     1   nonword       1.00       0
44        8     2   nonword       0.75       0
45        8     3   nonword       0.75       1
46        8     4   nonword       0.50       1
47        8     5   nonword       0.41       1
48        8     6   nonword       0.37       1
49        9     1   nonword       1.00       1
50        9     2   nonword       0.75       1
51        9     3   nonword       0.75       1
52        9     4   nonword       0.50       1
53        9     5   nonword       0.41       1
54        9     6   nonword       0.37       0
55       10     1   nonword       1.00       0
56       10     2   nonword       0.75       1
57       10     3   nonword       0.75       1
58       10     4   nonword       0.50       0
59       10     5   nonword       0.41       0
60       10     6   nonword       0.37       0
61       11     1   nonword       1.00       0
62       11     2   nonword       0.75       1
63       11     3   nonword       0.75       1
64       11     4   nonword       0.50       1
65       11     5   nonword       0.41       1
66       11     6   nonword       0.37       1
67       12     1   nonword       1.00       1
68       12     2   nonword       0.75       0
69       12     3   nonword       0.75       1
70       12     4   nonword       0.50       1
71       12     5   nonword       0.41       1
72       12     6   nonword       0.37       1
73       13     1  partword       1.00       1
74       13     2  partword       0.75       1
75       13     3  partword       0.75       1
76       13     4  partword       0.50       1
77       13     5  partword       0.41       0
78       13     6  partword       0.37       1
79       14     1  partword       1.00       1
80       14     2  partword       0.75       0
81       14     3  partword       0.75       1
82       14     4  partword       0.50       1
83       14     5  partword       0.41       0
84       14     6  partword       0.37       0
85       15     1  partword       1.00       1
86       15     2  partword       0.75       1
87       15     3  partword       0.75       1
88       15     4  partword       0.50       1
89       15     5  partword       0.41       0
90       15     6  partword       0.37       1
91       16     1  partword       1.00       1
92       16     2  partword       0.75       1
93       16     3  partword       0.75       1
94       16     4  partword       0.50       0
95       16     5  partword       0.41       1
96       16     6  partword       0.37       0
97       17     1  partword       1.00       1
98       17     2  partword       0.75       1
99       17     3  partword       0.75       0
100      17     4  partword       0.50       1
101      17     5  partword       0.41       1
102      17     6  partword       0.37       1
103      18     1  partword       1.00       0
104      18     2  partword       0.75       1
105      18     3  partword       0.75       1
106      18     4  partword       0.50       0
107      18     5  partword       0.41       0
108      18     6  partword       0.37       1
109      19     1  partword       1.00       1
110      19     2  partword       0.75       0
111      19     3  partword       0.75       0
112      19     4  partword       0.50       1
113      19     5  partword       0.41       1
114      19     6  partword       0.37       0
115      20     1  partword       1.00       0
116      20     2  partword       0.75       0
117      20     3  partword       0.75       1
118      20     4  partword       0.50       0
119      20     5  partword       0.41       0
120      20     6  partword       0.37       1
121      21     1  partword       1.00       1
122      21     2  partword       0.75       1
123      21     3  partword       0.75       1
124      21     4  partword       0.50       1
125      21     5  partword       0.41       0
126      21     6  partword       0.37       1
127      22     1  partword       1.00       0
128      22     2  partword       0.75       1
129      22     3  partword       0.75       0
130      22     4  partword       0.50       0
131      22     5  partword       0.41       1
132      22     6  partword       0.37       1
133      23     1  partword       1.00       0
134      23     2  partword       0.75       1
135      23     3  partword       0.75       0
136      23     4  partword       0.50       0
137      23     5  partword       0.41       1
138      23     6  partword       0.37       0
139      24     1  partword       1.00       0
140      24     2  partword       0.75       1
141      24     3  partword       0.75       1
142      24     4  partword       0.50       0
143      24     5  partword       0.41       0
144      24     6  partword       0.37       1
> 
> #let's explore SummaryBy
> library("doBy")
> summaryBy(correct~subject,data=exp1,FUN=mean,keep.names=TRUE)
   subject   correct
1        1 0.8333333
2        2 0.6666667
3        3 0.6666667
4        4 0.8333333
5        5 0.6666667
6        6 0.6666667
7        7 0.8333333
8        8 0.6666667
9        9 0.8333333
10      10 0.3333333
11      11 0.8333333
12      12 0.8333333
13      13 0.8333333
14      14 0.5000000
15      15 0.8333333
16      16 0.6666667
17      17 0.8333333
18      18 0.5000000
19      19 0.5000000
20      20 0.3333333
21      21 0.8333333
22      22 0.5000000
23      23 0.3333333
24      24 0.5000000
> #compare the above with 
> summaryBy(correct~condition+subject,data=exp1,FUN=mean,keep.names=TRUE)
   condition subject   correct
1    nonword       1 0.8333333
2    nonword       2 0.6666667
3    nonword       3 0.6666667
4    nonword       4 0.8333333
5    nonword       5 0.6666667
6    nonword       6 0.6666667
7    nonword       7 0.8333333
8    nonword       8 0.6666667
9    nonword       9 0.8333333
10   nonword      10 0.3333333
11   nonword      11 0.8333333
12   nonword      12 0.8333333
13  partword      13 0.8333333
14  partword      14 0.5000000
15  partword      15 0.8333333
16  partword      16 0.6666667
17  partword      17 0.8333333
18  partword      18 0.5000000
19  partword      19 0.5000000
20  partword      20 0.3333333
21  partword      21 0.8333333
22  partword      22 0.5000000
23  partword      23 0.3333333
24  partword      24 0.5000000
> #and with
> summaryBy(correct~condition,data=exp1,FUN=mean,keep.names=TRUE)
  condition   correct
1   nonword 0.7222222
2  partword 0.5972222
> 
> nonword_data <- exp1[1:72, 5]
> partword_data <- exp1[73:144, 5]
> 	
> #t-test for nonword condition against chance
> t.test(nonword_data, mu=.5, alternative="greater")

	One Sample t-test

data:  nonword_data
t = 4.1805, df = 71, p-value = 4.089e-05
alternative hypothesis: true mean is greater than 0.5
95 percent confidence interval:
 0.6336319       Inf
sample estimates:
mean of x 
0.7222222 

> 
> #t-test for part-word condition against chance
> t.test(partword_data, mu=.5, alternative="greater")

	One Sample t-test

data:  partword_data
t = 1.6703, df = 71, p-value = 0.04963
alternative hypothesis: true mean is greater than 0.5
95 percent confidence interval:
 0.5002153       Inf
sample estimates:
mean of x 
0.5972222 

> 
> #t-test for nonword condition vs. part-word condition (not in the paper but seems like a good idea!)
> t.test(nonword_data, partword_data, alternative="two.sided")

	Welch Two Sample t-test

data:  nonword_data and partword_data
t = 1.5858, df = 140.85, p-value = 0.115
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -0.03083576  0.28083576
sample estimates:
mean of x mean of y 
0.7222222 0.5972222 

> #six t-tests to determine which of the "words" are above chance (think about how to use the 'trial' column)
> trial1 <- exp1[exp1$trial==1, 5]
> trial2 <- exp1[exp1$trial==2, 5]
> trial3 <- exp1[exp1$trial==3, 5]
> trial4 <- exp1[exp1$trial==4, 5]
> trial5 <- exp1[exp1$trial==5, 5]
> trial6 <- exp1[exp1$trial==6, 5]
> 
> trial1
 [1] 1 1 0 0 0 1 1 0 1 0 0 1 1 1 1 1 1 0 1 0 1 0 0 0
> t.test(trial1, mu=.5, alternative="greater")

	One Sample t-test

data:  trial1
t = 0.40105, df = 23, p-value = 0.346
alternative hypothesis: true mean is greater than 0.5
95 percent confidence interval:
 0.3636047       Inf
sample estimates:
mean of x 
0.5416667 

> t.test(trial2, mu=.5, alternative="greater")

	One Sample t-test

data:  trial2
t = 2.1982, df = 23, p-value = 0.01913
alternative hypothesis: true mean is greater than 0.5
95 percent confidence interval:
 0.5458995       Inf
sample estimates:
mean of x 
0.7083333 

> t.test(trial3, mu=.5, alternative="greater")

	One Sample t-test

data:  trial3
t = 2.7689, df = 23, p-value = 0.005461
alternative hypothesis: true mean is greater than 0.5
95 percent confidence interval:
 0.5952556       Inf
sample estimates:
mean of x 
     0.75 

> t.test(trial4, mu=.5, alternative="greater")

	One Sample t-test

data:  trial4
t = 0.81064, df = 23, p-value = 0.2129
alternative hypothesis: true mean is greater than 0.5
95 percent confidence interval:
 0.4071491       Inf
sample estimates:
mean of x 
0.5833333 

> t.test(trial5, mu=.5, alternative="greater")

	One Sample t-test

data:  trial5
t = 1.6956, df = 23, p-value = 0.05173
alternative hypothesis: true mean is greater than 0.5
95 percent confidence interval:
 0.4982023       Inf
sample estimates:
mean of x 
0.6666667 

> t.test(trial6, mu=.5, alternative="greater")

	One Sample t-test

data:  trial6
t = 2.1982, df = 23, p-value = 0.01913
alternative hypothesis: true mean is greater than 0.5
95 percent confidence interval:
 0.5458995       Inf
sample estimates:
mean of x 
0.7083333 

> #This shows that trials 2, 3, and 6 are operating above chance, trials1 and 4 are not significantly above chance, and trial 5 is close to being significant but does not quite meet the p = .05 threshold.
> 
> #t-test comparing subject performance on 3 words with highest transitional probability against other three words
> 
> highProb <- c(exp1[exp1$trial==1, 5], exp1[exp1$trial==2, 5], exp1[exp1$trial==3, 5])
> lowProb <- c(exp1[exp1$trial==4, 5], exp1[exp1$trial==5, 5], exp1[exp1$trial==6, 5])
> 
> #I'll do 3 t tests: One comparing high transition probability against chance, one comparing low transition probability against chance, and one comparing high transition probability against low transition probability.
> 
> t.test(highProb, mu=.5, alternative="greater")

	One Sample t-test

data:  highProb
t = 2.9791, df = 71, p-value = 0.001977
alternative hypothesis: true mean is greater than 0.5
95 percent confidence interval:
 0.573428      Inf
sample estimates:
mean of x 
0.6666667 

> t.test(lowProb, mu=.5, alternative="two.sided")

	One Sample t-test

data:  lowProb
t = 2.704, df = 71, p-value = 0.008569
alternative hypothesis: true mean is not equal to 0.5
95 percent confidence interval:
 0.5401177 0.7654379
sample estimates:
mean of x 
0.6527778 

> t.test(highProb, lowProb, alternative="two.sided")

	Welch Two Sample t-test

data:  highProb and lowProb
t = 0.17468, df = 141.99, p-value = 0.8616
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -0.1432928  0.1710706
sample estimates:
mean of x mean of y 
0.6666667 0.6527778 

> #It seems that the words with higher and lower transitional probability are operating at about the same rate above chance.
> 
> #I could not for the life of me figure out the code for exp2 :(